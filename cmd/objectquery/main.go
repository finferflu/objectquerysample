package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strings"

	"objectquery/internal/objquery"
)

func main() {
	var keys string

	flag.StringVar(&keys, "k", "", "A list of keys separated by /, e.g. key1/key2/key3")

	flag.Parse()
	if keys == "" {
		fmt.Println("Please provide at least one key")
		os.Exit(1)
	}

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()

	o, err := objquery.NewObjQuery(scanner.Text())
	if err != nil {
		fmt.Println("Could not parse the provided object: " + err.Error())
		os.Exit(1)
	}

	val, err := o.GetValue(strings.Split(keys, "/"))
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println(val)
}
