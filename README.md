# objectQuerySample

A simple script that takes a JSON object as STDIN and a key tree, and returns its value in the JSON object, if it exists. For example:

```sh
  $ jq -rc . test/fixtures/test.json | docker run -i --rm registry.gitlab.com/finferflu/objectquerysample -k a/b/c
  d
```
