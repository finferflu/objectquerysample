FROM golang:1.16

ADD bin/objectquery /usr/bin/objectquery

ENTRYPOINT [ "/usr/bin/objectquery" ]