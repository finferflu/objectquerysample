package objquery

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetValue(t *testing.T) {
	str := `{
		"a": {
			"b": {
				"c": "d",
				"f": "g"
			}
		}
	}`

	o, err := NewObjQuery(str)
	assert.Nil(t, err)
	val, err := o.GetValue([]string{"a", "b", "c"})
	assert.Nil(t, err)
	assert.Equal(t, "d", val)

	_, err = o.GetValue([]string{"a", "b", "h"})
	if assert.NotNil(t, err) {
		assert.Equal(t, "key not found: h", err.Error())
	}

	_, err = o.GetValue([]string{"a", "b", "c", "d"})
	if assert.NotNil(t, err) {
		assert.Equal(t, "expecting a nested structure, but found a string", err.Error())
	}
}
