package objquery

import (
	"encoding/json"
	"errors"
	"fmt"
)

type ObjQuery struct {
	JSON map[string]interface{}
}

func NewObjQuery(j string) (o ObjQuery, err error) {
	err = json.Unmarshal([]byte(j), &o.JSON)
	if err != nil {
		return
	}

	return
}

func (o ObjQuery) GetValue(keys []string) (val interface{}, err error) {
	var ok bool

	if len(keys) < 1 {
		return "", errors.New("at least one key needs to be provided")
	}

	if val, ok = o.JSON[keys[0]]; !ok {
		return "", errors.New("key not found: " + keys[0])
	} else if len(keys) == 1 {
		// return the value if this is the last key
		return
	} else if o.JSON, ok = val.(map[string]interface{}); !ok {
		// if this isn't the last value, then we're expecting a map
		return "", fmt.Errorf("expecting a nested structure, but found a string")
	} else {
		// recurse if there are more keys
		return o.GetValue(keys[1:])
	}
}
